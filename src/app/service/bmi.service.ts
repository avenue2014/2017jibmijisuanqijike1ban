import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BMIForm} from "../vo/BMIForm";

@Injectable({
  providedIn: 'root'
})
export class BmiService {

  constructor(private http:HttpClient) { }


  private getBMIValueUrl="http://localhost/getBMIValue";

  getBMIValue(bmiform:BMIForm){

    return this.http.post(this.getBMIValueUrl,bmiform).toPromise()

  }



}
