import { Component, OnInit } from '@angular/core';
import {BMIForm} from "../vo/BMIForm";
import {ResultVo} from "../vo/ResultVo";
import {BmiService} from "../service/bmi.service";

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.css']
})
export class BmiComponent implements OnInit {
  bmiform:BMIForm;
  resultvo:ResultVo;
  imageurl:string;
  constructor(private bmiservice:BmiService) {
    this.bmiform=new BMIForm();
    this.resultvo=new ResultVo();
  }

  ngOnInit() {
  }

  getBMIValue(){
    console.log("你的身高是"+this.bmiform.sg+"你的体重是"+this.bmiform.tz);

    this.bmiservice.getBMIValue(this.bmiform)
      .then((data:any)=>{
        console.dir(data);
        this.resultvo=data;
        this.getImageUrl()
      })




  }

  getImageUrl(){
    switch (this.resultvo.state){
      case "偏瘦":
        this.imageurl= "/assets/images/thin.jpg";
        break

      case "正常":
        this.imageurl= "/assets/images/normal.jpg";
        break

      case "过重":
        this.imageurl= "/assets/images/babyfat.jpg";
        break

      case "肥胖":
        this.imageurl= "/assets/images/fat.jpg";
        break



    }


  }


}
